
public class FamilyTree {
	public FamilyTree()
	{
		ancestor = new FamilyTreeNode();
		ancestor.partner = new FamilyTreeNode();
		ancestor.partner.partner = ancestor;
	}
	private class FamilyTreeNode
	{

		public FamilyTreeNode(){
			this.sibling = null;
			this.child = null;
			this.partner = null;
			this.name = Input.getString("Enter family member's name: ");
		}
		public void addSibling(){
			//recursive algorithm
			//if a sibling exists
			//tell that sibling to add another sibling
			//else
			//make current sibling ref a new sibling
			if(sibling != null)
			{
				this.sibling.addSibling();
			}
			else
			{
				this.sibling = new FamilyTreeNode();
			}
		
		}
		public void display()
		{
			//display this.name
			//display partner name
			//tell children to display names
			//tell siblings to display their names
			if(this.partner!=null)
			{
				System.out.println(this.name +" partner " + this.partner.name);
			}
			else
			{
				System.out.println(this.name);
			}
			if(child != null)
			{
				this.child.display();
			}
			//else
			//{
			//	System.out.println("no children");
			//}
			if (this.sibling != null)
			{
				this.sibling.display();
			}
			
		}
		public String name;
		public FamilyTreeNode partner;
		public FamilyTreeNode sibling;
		public  FamilyTreeNode child;
	}
	private FamilyTreeNode ancestor;
	public void addChild()
	{
		//algorithm
		// if ancestor has a partner, adding a child is allowed
		// if ancestor already has a child, tell that child to add sibling
		// else
		// add a new child to ancestor and make partner point to same child
		if(ancestor.partner != null)
		{
			if(ancestor.child!=null)
			{
				ancestor.child.addSibling();
			}
			else
			{
				ancestor.child = new FamilyTreeNode();
				ancestor.partner.child = ancestor.child;
			}
		}
	}
	public void display()
	{
		ancestor.display();
		ancestor.partner.display();
	}
}

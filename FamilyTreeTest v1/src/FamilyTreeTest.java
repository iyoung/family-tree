
public class FamilyTreeTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FamilyTree family = new FamilyTree();
		boolean quit = false;
				while(!quit)
				{
					System.out.println("1: Add a child");
					System.out.println("2: display");
					System.out.println("3: quit");
					Integer choice = Input.getInteger("Choose an option");
					if(choice == 1)
					{
						family.addChild();
					}
					if(choice == 2)
					{
						family.display();
					}
					if(choice == 3)
					{
						quit = true;
					}
				}

	}

}

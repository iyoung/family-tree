
public class FamilyTree {
	public FamilyTree()
	{
		nextID = 1;
		ancestor = new FamilyTreeNode(nextID);
		ancestor.partner.partner = ancestor;
		nextID++;
	}
	private class FamilyTreeNode
	{

		public FamilyTreeNode(int uniqueID){
			id = uniqueID;
			this.sibling = null;
			this.child = null;
			this.partner = null;
			this.name = Input.getString("Enter family member's name: ");
		}
		public FamilyTreeNode getNodeByID(int selection){
			//algorithm for search and retrieve
			//if current node id matches selection
			//get a reference to this node
			//if partner exists and matches selection
			//get reference to partner node.
			//if node has siblings
			//tell sibling to search
			//if returned node is null
			//tell child to search
			//if found get reference
			FamilyTreeNode tempNode = null;
			if(this.id==selection)
			{
				tempNode = this;
			}
			else
			{
				if(this.partner!=null)
				{
					if(partner.id == selection)
					{
						tempNode = this.partner;
					}
					else
					{
						
						if(tempNode == null)
						{
							if(this.sibling!=null)
							{
								tempNode = this.sibling.getNodeByID(selection);
							}
							if(tempNode == null)
							{
							
								if(this.child!=null)
								{
									tempNode = this.child.getNodeByID(selection);
								}
							}
						}
					}
				}
			}
			return tempNode;
			
		}
		public void addSibling(int uniqueID){
			//recursive algorithm
			//if a sibling exists
			//tell that sibling to add another sibling
			//else
			//make current sibling ref a new sibling
			if(sibling != null)
			{
				this.sibling.addSibling(uniqueID);
			}
			else
			{
				this.sibling = new FamilyTreeNode(uniqueID);
			}
		
		}
		public void display()
		{
			//display this.name
			//display partner name
			//tell children to display names
			//tell siblings to display their names
			if(this.partner!=null)
			{
				System.out.println(this.name +" (identifier "+(char)this.id+ ")"+" partner " + this.partner.name +"(identifier "+this.partner.id+")");
			}
			else
			{
				System.out.println(this.name+" (identifier "+(char)this.id+ ")");
			}
			if(child != null)
			{
				this.child.display();
			}
			else
			{
				System.out.println("no children");
			}
			if (this.sibling != null)
			{
				this.sibling.display();
			}
			
		}
		public String name;
		public int id;
		public FamilyTreeNode partner;
		public FamilyTreeNode sibling;
		public  FamilyTreeNode child;
	}
	private FamilyTreeNode ancestor;
	private int nextID;
	public void addChild()
	{
		//algorithm
		// if ancestor has a partner, adding a child is allowed
		// if ancestor already has a child, tell that child to add sibling
		// else
		// add a new child to ancestor and make partner point to same child
		if(ancestor.partner != null)
		{
			if(ancestor.child!=null)
			{
				ancestor.child.addSibling(nextID);
				nextID++;
			}
			else
			{
				ancestor.child = new FamilyTreeNode(nextID);
				ancestor.partner.child = ancestor.child;
				nextID++;
			}
		}
	}
	public void addPartner()
	{
		displayFamily();
		int choice = Input.getInteger("Enter an ID to add child to: ");
		FamilyTreeNode tempNode = ancestor.getNodeByID(choice);
		if (tempNode != null)
		{
			if(tempNode.partner==null)
			{
				tempNode.partner = new FamilyTreeNode(nextID);
				nextID++;
			}
			else
			{
				System.out.println("This family member already has a partner");
			}
		}
		else
		{
			System.out.println("Family member not found");
		}
	}
	private void displayFamily()
	{
		ancestor.display();
	}
	private void displayFamilyMember()
	{
		//algorithm
		//get input
		//search for node with id of input
		//if found
		//display member and all siblings/partner/children
		//else
		//inform user of failed search
		int choice = Input.getInteger("Select a family member ID to display: ");
		FamilyTreeNode member = ancestor.getNodeByID(choice);
		if(member != null)
		{
			member.display();
		}
		else
		{
			System.out.println("Family member not found");
		}
	}
	public void display()
	{
		System.out.println("1: Display entire family");
		System.out.println("2: Display by ID");
		int choice = Input.getInteger("Selected an option");
		if (choice == 1)
		{
			displayFamily();
		}
		if(choice == 2)
		{
			displayFamilyMember();
		}
	}
	
}
